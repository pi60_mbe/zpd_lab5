﻿using System;
using ZPD_Lab_1_5.Alphabets;
using System.Numerics;


namespace ZPD_Lab_1_5
{
    class Program
    {
        static void Main(string[] args)
        {
           
            //El-GAMAl
            int p = 43;
            int q = 13;
            int ka = 16;
            int kb = 18;

            int language;
            EnglishAlphabet eng = new EnglishAlphabet();
            UkrainianAlphabet ukr = new UkrainianAlphabet();
            RSACipher rsa;
            ElGamalCipher el;


            Console.Write("Message->");

            char[] message = Console.ReadLine().ToCharArray();
            Console.Write("Language->");
            language = Convert.ToInt32(Console.ReadLine());
            if(language == 0)
            {
                rsa = new RSACipher(ukr);
                el = new ElGamalCipher(p, q, ka, kb, ukr);
            }
            else
            {
                rsa = new RSACipher(eng);
                el = new ElGamalCipher(p, q, ka, kb, eng);
            }

            Console.Write("Encrypt/Decrypt(0/1)->");
            int type = Convert.ToInt32(Console.ReadLine());

            if (type == 0)
            {
                Console.WriteLine(Writestring(rsa.Encode(message)));
                Console.WriteLine(Writestring(el.Encode(message)));
            }
            else
            {
                Console.WriteLine(rsa.Decode(TointArr(message)));
                Console.WriteLine(el.Decode(TointArr(message)));
            }





        }

        public static int [] TointArr(char [] arr)
        {
            int count = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == ' ')
                    count++;
            }

            int[] res = new int[count+1];
            string temp = null;

            for(int i =0, j=0; i < arr.Length; i++,j++)
            {
                while (arr[i] != ' ')
                {
                    temp += arr[i].ToString();
                    i++;

                }
                res[j] = Convert.ToInt32(temp);
            }
            return res;
        }

        public static string Writestring(BigInteger[] arr)
        {
            string res = null;

            for (int i = 0; i < arr.Length; i++)
            {
                res += arr[i].ToString()+" ";
                
            }
            return res;
        }
        public static string Writestring(int[] arr)
        {
            string res = null;

            for (int i = 0; i < arr.Length; i++)
                res += arr[i].ToString()+ " ";
            return res;
        }

    }
}
